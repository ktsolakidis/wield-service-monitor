#!/bin/bash

LOG_FILE="./config.log"
WAIT_TIME=120

while true; do
    current_time=$(date +"%Y-%m-%d %T")

    # checking in the config.log file if the word finalized appears in the last 500 rows
    if ! tail -n 500 "$LOG_FILE" | grep -q "finalized"; then
        echo "[$current_time] [INFO] - The word 'finalized' not found in the last 500 log rows. Restarting wield-service..."
        sudo systemctl restart wield
        sleep $WAIT_TIME
    else
        echo "[$current_time] [INFO] - Waiting for the 120 seconds to start checking again..."
        sleep $WAIT_TIME
    fi
done
